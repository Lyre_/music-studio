import path from 'path';
import { defineConfig } from 'vite'
// @ts-ignore
import react from '@vitejs/plugin-react-swc';

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    port: 3005
  },
  resolve: {
    alias: {
      "@app": path.resolve(__dirname, './src/01-app'),
      "@processes": path.resolve(__dirname, './src/02-processes'),
      "@pages": path.resolve(__dirname, './src/03-pages'),
      "@widgets": path.resolve(__dirname, './src/04-widgets'),
      "@features": path.resolve(__dirname, './src/05-features/'),
      "@entities": path.resolve(__dirname, './src/06-entities'),
      "@shared": path.resolve(__dirname, './src/07-shared'),
      "@scss": path.resolve(__dirname, './src/07-shared/scss'),
      "@": path.resolve(__dirname, './src'),
    },
  },
  plugins: [react()],
})
