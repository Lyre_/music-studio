import ScrollToTop from '@app/helpers/scroll-to-top';
import { Box } from '@mui/material';
import Footer from '@processes/footer/ui/footer';
import Header from '../02-processes/header/ui/header';
import Routing from '../02-processes/routing/routing';
import { withHocs } from './hocs';
import c from './app.module.scss';

const App = () => {
  return (
    <>
      <ScrollToTop/>
      <Header />
      <Box component={'main'} className={c.main}>
        <Routing />
      </Box>
      <Footer />
    </>
  );
};

export default withHocs(App);
