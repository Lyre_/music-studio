import { createStore, Provider } from 'jotai';

export const globalStore = createStore()

export const withJotaiGlobalStore = (component: Component) => () =>
  <Provider store={globalStore}>{component()}</Provider>;
