import { createTheme, CssBaseline, GlobalStyles, StyledEngineProvider, ThemeProvider } from '@mui/material';

const globalStyles = (
  <GlobalStyles
    styles={{html: {fontSize: 10}, body: {fontSize: '1.6rem', background: '#f6f4f4'}}}
  />
);

export const withMui = (component: Component) => () =>
  (
    <ThemeProvider theme={theme}>
      <StyledEngineProvider injectFirst={true}>
        <CssBaseline/>
        {globalStyles}
        {component()}
      </StyledEngineProvider>
    </ThemeProvider>
  );

const theme = createTheme({
  palette: {
    error: {
      main: 'rgb(248, 35, 27)',
    }
  },
  typography: {
    body1: {
      fontSize: '1.6rem',
    },
    body2: {
      fontSize: '2.2rem',
    }
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 750,
      md: 900,
      lg: 1200,
      xl: 1536,
    },
  }
});
