import { compose } from 'ramda';
import { withJotaiGlobalStore } from './with-jotai-global-store';
import { withMui } from './with-mui';
import { withRouter } from './with-router';
import { withGlobalSuspense } from './witsh-global-suspense';

export const withHocs = compose(
  withMui,
  withGlobalSuspense,
  withJotaiGlobalStore,
  withRouter
);
