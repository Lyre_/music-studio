import { Suspense } from 'react';

export const withGlobalSuspense = (component: Component) => () =>
  <Suspense fallback={<div>endless loop or err !?!?!</div>}>{component()}</Suspense>;
