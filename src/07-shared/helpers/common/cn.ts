const cn = (...args: (string)[]) => {
  return args.join(' ');
};

export default cn;
