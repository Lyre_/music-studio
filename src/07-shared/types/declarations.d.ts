declare type Component<P = unknown> = (props?: P) => JSX.Element;
