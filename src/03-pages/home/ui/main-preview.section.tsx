import c from './main-previw.section.module.scss'
import mainPicSrc from '../../../07-shared/assets/1+Studio+Front+Blue+v1.3.jpg'

const MainPreviewSection = () => {
    return (
    <section className={c.root}>
      <img src={mainPicSrc} alt="pic" className={c.img}/>
    </section>
    );
};

export default MainPreviewSection;
