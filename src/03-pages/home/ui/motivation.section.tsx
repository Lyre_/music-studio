import { Stack, Typography } from '@mui/material';
import Grid2 from '@mui/material/Unstable_Grid2';
import ChelPicSrc from '@shared/assets/WytseSerieus.jpg';
import c from './motivation.section.module.scss';

const MotivationSection = () => {
  return (
    <Grid2 container component={'section'} p={2} className={c.root}>
      <Grid2 container justifyContent={'center'} alignItems={'center'}>
        <Grid2 p={2}>
          <img src={ChelPicSrc} alt={'pic'} className={c.img}/>
        </Grid2>
      </Grid2>
      <Grid2 container justifyContent={'center'}>
        <Stack component={Grid2} spacing={3} alignItems={'center'} justifyContent={'center'} textAlign={'center'} width={'70%'}>
          <Typography variant={'h3'} fontWeight={'bold'}>Let’s lift your music to a higher level!</Typography>
          <Typography variant={'body1'}>White Sea Studio is a down to earth, no nonsense and straightforward music
            studio. I do not join in the modern day “flashy quick fix” business that a big part of the music industry
            has become. I like to take the time to create an awesome sound, together with you.</Typography>
          <Typography variant={'body2'} fontWeight={'bold'}>Wytse Gerichhausen!</Typography>
        </Stack>
      </Grid2>
    </Grid2>
  );
};

export default MotivationSection;
