import MainPreviewSection from './main-preview.section';
import MotivationSection from './motivation.section';
import WhatWeAreSection from './what-we-are.section';

const HomePage = () => {
  return (
    <article>
      <MainPreviewSection/>
      <WhatWeAreSection/>
      <MotivationSection/>
    </article>
  );
};

export default HomePage;
