import { Button, Stack, Typography } from '@mui/material';
import Grid2 from '@mui/material/Unstable_Grid2';
import mixingPicSrc from '@shared/assets/mastering.png';
import masteringPicSrc from '@shared/assets/Mixingpng.png';
import advicePicSrc from '@shared/assets/RecordingGuidance.png';
import { atom, useAtomValue } from 'jotai';
import c from './what-we-are.section.module.scss';

interface IServiceCard {
  pic: { src: string, alt: string },
  title: string,
  text: string,
}

const serviceCardsAtom = atom<IServiceCard[]>([
  {title: 'Advice', text: 'Elevate your audio game!', pic: {src: advicePicSrc, alt: 'advice picture'},},
  {title: 'Mixing', text: 'Take your music to the next level!', pic: {src: mixingPicSrc, alt: 'mixing picture'},},
  {title: 'Mastering', text: 'The final polish!', pic: {src: masteringPicSrc, alt: 'mastering picture'},},
])

const WhatWeAreSection = () => {
  return (
    <Stack component={'section'} alignItems={'center'} spacing={1} py={4} px={2}>
      <Typography variant={'h1'}>White Sea Studio</Typography>
      <Typography variant={'h2'}>Making the world sound better!</Typography>
      <ServiceCards/>
    </Stack>
  );
};

const ServiceCards = () => {
  const cards = useAtomValue(serviceCardsAtom);

  return (
    <Grid2 container justifyContent={'space-evenly'} sx={{width: '100%'}}>
      {cards.map(card => {
        return (
          <Grid2 key={card.title} py={3}>
            <Stack spacing={2} alignItems={'center'}>
              <img src={card.pic.src} alt={card.pic.alt} className={c.imgRoot}/>
              <Button variant={'contained'} sx={{fontSize: '2rem', minWidth: 200, minHeight: 65}} color={'error'}>{card.title}</Button>
              <Typography variant={'body2'} pt={2}>{card.text}</Typography>
            </Stack>
          </Grid2>
        )
      })}
    </Grid2>
  )
}

export default WhatWeAreSection;
