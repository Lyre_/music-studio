import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import { ClickAwayListener, Grow, Link, MenuItem, MenuList, Paper, Popper, Stack } from '@mui/material';
import Grid2 from '@mui/material/Unstable_Grid2';
import { useCallback, useMemo, useRef, useState } from 'react';
import { NavLink as RouterLink, useHistory, useLocation } from 'react-router-dom';
import { SERVICE_PATH } from '../../routing/constants';
import { ILink } from '../lib/constants/links';
import c from './navbar.module.scss';

interface MenuProps {
  items: ILink[]
  name: string,
  path: string,
}

const compositionBtnID = Math.random().toString();
const compositionMenuID = Math.random().toString();


const PopupLinksMenu = ({items, name}: MenuProps) => {
  const history = useHistory();
  const location = useLocation();
  const [open, setOpen] = useState(false);
  const anchorRef = useRef<HTMLAnchorElement>(null);
  const isServicePath = useMemo(() => location.pathname.startsWith(SERVICE_PATH), [location.pathname]);
  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = useCallback(() => {

    setOpen(false);
  }, [setOpen]);

  const handleOpen = useCallback(() => {
    setOpen(true);
  }, [setOpen]);

  function handleListKeyDown(event: React.KeyboardEvent) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setOpen(false);
    } else if (event.key === 'Escape') {
      setOpen(false);
    }
  }

  const handleLink = useCallback((path: string) => {
    history.push(path);
    handleClose();
  }, [])

  return (
    <Grid2 onMouseLeave={handleClose}>
      <Link
        ref={anchorRef}
        id={compositionBtnID}
        aria-controls={open ? compositionMenuID : undefined}
        aria-expanded={open ? 'true' : undefined}
        aria-haspopup="true"
        onMouseEnter={handleOpen}
        className={isServicePath ? c.activeLink : c.link}
        underline={'hover'}
        onClick={handleToggle}
        sx={{display: 'flex', alignItems: 'center', cursor: 'pointer'}}
      >
        {name}
        <Stack sx={{height: '100%'}} justifyContent={'center'} pt={'2px'}>
          {open ? <KeyboardArrowUpIcon/> : <KeyboardArrowDownIcon/>}
        </Stack>
      </Link>
      <Popper
        open={open}
        anchorEl={anchorRef.current}
        role={undefined}
        placement="bottom"
        transition
        disablePortal
        onMouseLeave={handleClose}
        sx={{minWidth: '200px'}}
      >
        {({TransitionProps, placement}) => (
          <Grow
            {...TransitionProps}
            style={{
              transformOrigin:
                placement === 'bottom' ? 'left top' : 'left bottom',
            }}
          >
            <Paper>
              <ClickAwayListener onClickAway={handleClose}>
                <MenuList
                  autoFocusItem={open}
                  id={compositionMenuID}
                  aria-labelledby={compositionBtnID}
                  onKeyDown={handleListKeyDown}
                  sx={{fontSize: '1.4rem'}}
                >
                  {items.map((data) => (
                    <MenuItem onClick={() => handleLink(data.path)} key={data.path}>
                      <Link
                        className={c.link}
                        activeStyle={{color: 'var(--active-nav-link)'}}
                        exact
                        component={RouterLink}
                        underline={'hover'}
                        to={data.path}
                      >
                        {data.name}
                      </Link>
                    </MenuItem>)
                  )}

                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </Grid2>
  );
}

export default PopupLinksMenu;
