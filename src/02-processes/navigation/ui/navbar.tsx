import AppsIcon from '@mui/icons-material/Apps';
import CloseIcon from '@mui/icons-material/Close';
import { Button, Link, useMediaQuery, useTheme } from '@mui/material';
import Grid2 from '@mui/material/Unstable_Grid2';
import { useAtomValue, useSetAtom } from 'jotai';
import { useCallback } from 'react';
import { NavLink as RouterLink } from 'react-router-dom';
import { isNestedLink, links } from '../lib/constants/links';
import navigationModel from '../model/navigation.model';
import c from './navbar.module.scss';
import PopupLinksMenu from './popup-links-menu';

const {toggleMobileMenuAtom} = navigationModel;

const Navbar = (): JSX.Element => {
  const theme = useTheme();
  const isMobileView = useMediaQuery(theme.breakpoints.down('sm'))
  const setOpenMobileMenu = useSetAtom(toggleMobileMenuAtom);
  const isOpenMobileMenu = useAtomValue(toggleMobileMenuAtom);
  const normalLinksMenu = !isMobileView && (
    <Grid2 container spacing={2}>
      {links.map((data) => {
        if (isNestedLink(data)) {
          return (
            <PopupLinksMenu
              key={data.path}
              items={data.nested}
              name={data.name}
              path={data.path}
            />)
        }
        return (
          <Grid2 key={data.path}>
            <Link
              className={c.link}
              activeStyle={{color: '#0773cb'}}
              exact
              component={RouterLink}
              underline={'hover'}
              to={data.path}
            >
              {data.name}
            </Link>
          </Grid2>)
      })}
    </Grid2>)

  const handleClick = useCallback(() => {
    setOpenMobileMenu(prev => !prev);
  }, [setOpenMobileMenu]);

  const mobileLinksBtn = isMobileView && <Grid2 ml={'auto'} pr={1}>
    <Button onClick={handleClick} sx={{fontSize: 30}}>
      {isOpenMobileMenu ? <CloseIcon fontSize={'inherit'}/> : <AppsIcon fontSize={'inherit'}/>}
    </Button>
  </Grid2>

  return (
    isMobileView
      ? mobileLinksBtn
      : normalLinksMenu
  ) as JSX.Element
};


export default Navbar;
