import { KeyboardArrowRight } from '@mui/icons-material';
import { Button, Link, Stack, useMediaQuery, useTheme } from '@mui/material';
import Grid2 from '@mui/material/Unstable_Grid2';
import { atom, useAtomValue, useSetAtom } from 'jotai';
import { ReactNode, useEffect, useMemo } from 'react';
import { NavLink as RouterLink, useLocation } from 'react-router-dom';
import cn from '../../../07-shared/helpers/common/cn';
import { SERVICE_PATH } from '../../routing/constants';
import { ILink, isNestedLink, links } from '../lib/constants/links';
import navigationModel from '../model/navigation.model';
import c from './mobile-navbar.module.scss';
import classes from './navbar.module.scss';

const {visibleMobileMenuAtom, toggleMobileMenuAtom} = navigationModel;

const mobileLinksAtom = atom(links);
const prevMobileLinksAtom = atom<ILink[][]>([]);
const backMobileLinksAtom = atom(null, (get, set) => {
  const prev = get(prevMobileLinksAtom);
  const first = prev.splice(prev.length - 1, 1)[0]
  set(prevMobileLinksAtom, prev);
  set(mobileLinksAtom, !first ? mobileLinksAtom.init : first)
})
const currMobileLinksAtom = atom((get) => get(mobileLinksAtom), (get, set, val: ILink[]) => {
  const prevVal = get(mobileLinksAtom);
  set(prevMobileLinksAtom, prev => [...prev, prevVal]);
  set(mobileLinksAtom, val);
});

const MobileNavbar = () => {
  const theme = useTheme();
  const expLinks = useAtomValue(currMobileLinksAtom);
  const prevExpLinks = useAtomValue(prevMobileLinksAtom);
  const isVisible = useAtomValue(visibleMobileMenuAtom);
  const isSmView = useMediaQuery(theme.breakpoints.down('sm'));
  const setExpLinks = useSetAtom(currMobileLinksAtom);
  const setMobLinks = useSetAtom(mobileLinksAtom);
  const handleBackLinks = useSetAtom(backMobileLinksAtom);
  const setOpenMobileMenu = useSetAtom(toggleMobileMenuAtom);

  useEffect(() => {
    if (!isSmView) setOpenMobileMenu(false);
  }, [isSmView])
  useEffect(() => {
    if (isVisible === 'idle-menu') {
      const id = setTimeout(handleBackLinks, 300)
      return () => clearTimeout(id);
    }
  }, [setMobLinks, isVisible])

  return (
    <div className={c['mobile-nav-menu-wrapper']}>
      <Stack className={cn(c['mobile-nav-menu'], c[isVisible])} px={1} justifyContent={'center'} alignItems={'center'} textAlign={'center'}>
        <Stack>
          {prevExpLinks.length > 0 && <Button onClick={handleBackLinks}>Back</Button>}
          {expLinks.map((data) => {
            if (isNestedLink(data)) {
              return (
                <BigLink
                  key={data.path}
                  onClick={() => {
                    setExpLinks(data.nested)
                  }}
                >
                  {data.name}
                  <Stack sx={{height: '100%'}} justifyContent={'center'} pt={'2px'}>
                    {<KeyboardArrowRight/>}
                  </Stack>
                </BigLink>
              )
            }
            return (
              <Grid2 key={data.path}>
                <Link
                  className={classes.link}
                  activeStyle={{color: '#0773cb'}}
                  exact
                  component={RouterLink}
                  underline={'hover'}
                  to={data.path}
                  onClick={() => {
                    setOpenMobileMenu(false);
                  }}
                >
                  {data.name}
                </Link>
              </Grid2>)
          })}
        </Stack>
      </Stack>
    </div>
  );
};

interface Props {
  onClick?: (e: React.MouseEvent<HTMLAnchorElement, MouseEvent> | React.MouseEvent<HTMLSpanElement, MouseEvent>) => void
  children: ReactNode
}

const BigLink = ({onClick, children}: Props) => {
  const location = useLocation();
  const isServicePath = useMemo(() => location.pathname.startsWith(SERVICE_PATH), [location.pathname]);

  return <Link
    className={isServicePath ? classes.activeLink : classes.link}
    underline={'hover'}
    sx={{display: 'flex', alignItems: 'center', cursor: 'pointer'}}
    onClick={onClick}
  >
    {children}
  </Link>
}

/*  const util = {
    theme: useTheme(),
  }
  const st = {
    expLinks: useAtomValue(currMobileLinksAtom),
    prevExpLinks: useAtomValue(prevMobileLinksAtom),
    isVisible: useAtomValue(visibleMobileMenuAtom),
    isSmView: useMediaQuery(util.theme.breakpoints.down('sm')),
  }
  const cb = {
    setExpLinks : useSetAtom(currMobileLinksAtom),
    setMobLinks: useSetAtom(mobileLinksAtom),
    handleBackLinks: useSetAtom(backMobileLinksAtom),
    setOpenMobileMenu: useSetAtom(toggleMobileMenuAtom),
  }*/
export default MobileNavbar;
