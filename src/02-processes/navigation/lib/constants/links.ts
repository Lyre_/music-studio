import {
  ABOUT_PATH,
  CONTACT_PATH,
  FAQ_PATH,
  LISTEN_PATH,
  SERVICE_ADVICE_PATH,
  SERVICE_MASTERING_PATH,
  SERVICE_MIXING_PATH,
  SERVICE_PATH
} from '../../../routing/constants';

export interface ILink {
  name: string,
  path: string
}

type DeepLink = ILink & { nested: ILink[] }
export type INestedLink = ILink | DeepLink
export const isNestedLink = (link: INestedLink): link is DeepLink => Boolean((link as DeepLink)?.nested);

export const links: INestedLink[] = [
  {name: 'About', path: ABOUT_PATH},
  {name: 'Listen', path: LISTEN_PATH},
  {
    name: 'Services',
    path: SERVICE_PATH,
    nested: [
      {name: 'Advice', path: SERVICE_ADVICE_PATH},
      {name: 'Mixing', path: SERVICE_MIXING_PATH},
      {name: 'Mastering', path: SERVICE_MASTERING_PATH},
    ]
  },
  {name: 'FAQ', path: FAQ_PATH},
  {name: 'Contact', path: CONTACT_PATH},
]
