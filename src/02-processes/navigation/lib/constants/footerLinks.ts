import { ILink } from '@processes/navigation/lib/constants/links';
import { DISCLAIMER_PATH, PRIVACY_POLICY_PATH, TERMS_OF_SERVICE_PATH } from '@processes/routing/constants';

const links: ILink[] = [
  {name: 'Privacy policy', path: PRIVACY_POLICY_PATH},
  {name: 'Disclaimer', path: DISCLAIMER_PATH},
  {name: 'Terms of service', path: TERMS_OF_SERVICE_PATH},
]
export default links
