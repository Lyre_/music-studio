import { atom } from 'jotai';

const toggleMobileMenuAtom = atom(false);
const visibleMobileMenuAtom = atom(get => {
  return get(toggleMobileMenuAtom) ? 'finished-menu' : 'idle-menu'
});

export default {
  toggleMobileMenuAtom,
  visibleMobileMenuAtom,
}
