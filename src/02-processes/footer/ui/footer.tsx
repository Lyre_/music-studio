import { Link, Stack } from '@mui/material';
import Grid2 from '@mui/material/Unstable_Grid2';
import footerLinks from '@processes/navigation/lib/constants/footerLinks';
import { Link as RouterLink } from 'react-router-dom';
import c from './footer.module.scss';
import YouTubeIcon from '@mui/icons-material/YouTube';
import MailIcon from '@mui/icons-material/Mail';

const Footer = () => {
  return (
    <Stack className={c.root} spacing={1} p={2} justifyContent={'center'} alignItems={'center'}>
      <Grid2 container spacing={2}>
        <Grid2><YouTubeIcon/></Grid2>
        <Grid2><MailIcon/></Grid2>
      </Grid2>
      <Links/>
    </Stack>
  );
};

const divider = <Grid2>-</Grid2>

const Links = () => {
  return (
    <Grid2 container spacing={1}>
      {footerLinks.map((l, idx) => {
        return <>
          <Grid2>
            <Link component={RouterLink} to={l.path} color={'error'}>{l.name}</Link>
          </Grid2>
          {footerLinks.length - 1 !== idx && divider}
        </>
      })}

    </Grid2>
  )
}

export default Footer;
