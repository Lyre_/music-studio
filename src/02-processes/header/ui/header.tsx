import { Box } from '@mui/material';
import Grid2 from '@mui/material/Unstable_Grid2';
import { Link } from 'react-router-dom';
import MobileNavbar from '../../navigation/ui/mobile-navbar';
import Navbar from '../../navigation/ui/navbar';
import { HOME_PATH } from '../../routing/constants';
import c from './header.module.scss';


const Header = () => {

  return (
    <>
      <header className={c.root}>
        <Box component={Link} to={HOME_PATH} px={2}>logo</Box>
        <Grid2 container sx={{flexGrow: 2}} justifyContent={'center'}>
          <Navbar/>
        </Grid2>
      </header>
      <div className={c.offset}>
        <MobileNavbar/>
      </div>
    </>
  );
};

export default Header;
