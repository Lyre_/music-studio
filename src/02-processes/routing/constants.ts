export const HOME_PATH = '/'
export const ABOUT_PATH = '/about'
export const LISTEN_PATH = '/listen'
export const SERVICE_PATH = '/service'
export const SERVICE_ADVICE_PATH = SERVICE_PATH + '/advice'
export const SERVICE_MIXING_PATH = SERVICE_PATH + '/mixing'
export const SERVICE_MASTERING_PATH = SERVICE_PATH + '/mastering'
export const PRIVACY_POLICY_PATH = '/privacy-policy'
export const DISCLAIMER_PATH = '/disclaimer'
export const TERMS_OF_SERVICE_PATH = '/terms-of-service'
export const FAQ_PATH = '/faqs'
export const CONTACT_PATH = '/contact'

const IMG_BASE_URL = 'http://localhost:4005/wallpapers/';//http://localhost:4005/wallpapers/Preview+Studio+Front+Blue.jpg?test=true
//https://images.squarespace-cdn.com/content/v1/5fe20e09f1a8e9646dbecf96/7f5a8511-4ae3-4b44-9197-d39681630ab0/1+Studio+Front+Blue+v1.3.jpg?format=750w
