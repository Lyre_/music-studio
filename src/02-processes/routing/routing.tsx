import { Redirect, Route, Switch } from 'react-router-dom';
import HomePage from '../../03-pages/home/ui/home.page';
import {
  ABOUT_PATH,
  CONTACT_PATH, DISCLAIMER_PATH,
  FAQ_PATH, HOME_PATH,
  LISTEN_PATH, PRIVACY_POLICY_PATH,
  SERVICE_ADVICE_PATH,
  SERVICE_MASTERING_PATH,
  SERVICE_MIXING_PATH, SERVICE_PATH, TERMS_OF_SERVICE_PATH
} from './constants';


const Routing = () => {
  return (
    <Switch>
      <Route path={LISTEN_PATH}>
        page2
      </Route>
      <Route path={SERVICE_ADVICE_PATH}>
        page advice
      </Route>
      <Route path={SERVICE_MIXING_PATH}>
        page3
      </Route>
      <Route path={SERVICE_MASTERING_PATH}>
        page4
      </Route>
      <Route path={SERVICE_PATH}>
        <Redirect to={SERVICE_ADVICE_PATH} />
      </Route>
      <Route path={FAQ_PATH}>
        page5
      </Route>
      <Route path={CONTACT_PATH}>
        page6
      </Route>
      <Route path={PRIVACY_POLICY_PATH}>
        Privacy policy
      </Route>
      <Route path={DISCLAIMER_PATH}>
        Disclaimer
      </Route>
      <Route path={TERMS_OF_SERVICE_PATH}>
        Terms of service
      </Route>
      <Route path={ABOUT_PATH}>
        about
      </Route>
      <Route path={HOME_PATH}>
        <HomePage/>
      </Route>
    </Switch>
  );
};

export default Routing;
